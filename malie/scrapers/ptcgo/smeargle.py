#!/usr/bin/env python3
import argparse
import json
import logging
import os
import re
import sys
import traceback

import unitypack
from PIL import ImageOps

from malie.lib.cache import Cache
from malie.lib import WebScraper
from malie.scrapers.ptcgo import (
    crop,
    logger,
)
# The following import is included at point-of-use below to avoid
# problems with cyclic imports.
# from malie.scrapers.ptcgo import safarizone

LOGGER = logger.get_logger()

DRAGONITE_TRACEBACK = None
DRAGONITE = False

try:
    from malie.scrapers.ptcgo.dragonite import Dragonite
    DRAGONITE = True
except ModuleNotFoundError:
    DRAGONITE_TRACEBACK = sys.exc_info()


class UnityBundle:
    def __init__(self, file=None, filename=None):
        self._fh = file or open(filename, "rb")
        self._uobj = unitypack.load(self._fh)
        self.object_map = {}
        self.digest()

    def digest(self):
        self.object_map = {}
        for asset in self._uobj.assets:
            for obj in asset.objects.values():
                if obj.type != 'AssetBundle':
                    continue
                info = obj.read()
                for k, v in info.get('m_Container', {}):
                    self.object_map[k] = v['asset']

    def dump(self):
        for name, ptr in self.object_map.items():
            if ptr.object.type != 'Texture2D':
                continue
            img = self.get(name)
            with open("{}.png".format(name), "wb") as fp:
                img.save(fp)

    def get(self, name, strict=True):
        obj = self.object_map[name]
        if obj is None:
            raise ValueError("Object %s is None" % name)
        obj = obj.object
        if obj.type != 'Texture2D':
            raise ValueError("Object %s is not Texture2D (got %s)" % (name, obj.type))
        tex = obj.read()
        if strict:
            assert tex.name == name
        img = ImageOps.flip(tex.image)
        return img

    def verify(self, crc32):
        if not (hasattr(self._uobj, 'headerless_crc') or
                hasattr(self._uobj, 'headered_crc')):
            msg = """
The version of unitypack that is installed cannot perform CRC
checks. Please see
https://gitlab.com/malie-library/malie/blob/master/README.rst for
information on how to install the malie fork of unitypack which
provides this function. Otherwise, you can use `--validate none` to
skip these checks."""
            raise NotImplementedError(msg)

        if self._uobj.headerless_crc == crc32:
            return True

        if self._uobj.headered_crc == crc32:
            return True

        # Oh dear.
        LOGGER.warning("CRC Mismatch; manifest: %x, headered: %x, headerless: %x",
                       crc32, self._uobj.headered_crc, self._uobj.headerless_crc)
        return False

    def close(self):
        self._fh.close()


class Ubcache:
    def __init__(self):
        self._cache = {}

    def flush(self):
        for ub in self._cache.values():
            ub.close()
        self._cache = {}

    def fetch(self, filename, fetcher):
        if filename not in self._cache:
            tmp = fetcher()
            self._cache[filename] = tmp
        return self._cache[filename]


class Smeargle:
    """
    Smeargle takes a PTCGO asset manifest and uses it to download art assets.

    With the addition of card and localization information,
    Smeargle can extract the textures themselves with human-readable names.

    :param cachedir: Path to cached Unity bundle objects.
                     Defaults to Kecleon's {datapath}/bundles.
    :param datadir: Path to extracted textures.
                    Will default to Kecleon's {datapath} if set, or
                    $XDG_DATA_HOME/smeargle otherwise.
    """
    def __init__(self, cachedir=None, datadir=None, manifest=None, validate=None):
        from malie.scrapers.ptcgo import safarizone
        sfz = safarizone.get_safari_zone()
        self.sfz = sfz
        self.sdl = sfz.sdm
        self.failures = []
        self._manifest_vers = manifest
        self.validate = validate

        # Todo: replace WebScraper with Spinarak
        self.ws = WebScraper.WebScraper()
        self.ws.headers = {
            'User-Agent': 'UnityPlayer/%s (http://unity3d.com)' % (
                sfz.cfg['unityVersion']),
            'X-Unity-Version': sfz.cfg['unityVersion']
        }
        self._dragonite = None

        if cachedir:
            self.cache = Cache('smeargle:bundles', cachedir)
        else:
            self.cache = sfz.cfg.cache.bundles

        datadir = datadir or sfz.cfg.get('datapath')
        self.data = Cache('smeargle', datadir, data=True)
        self.data = self.data.subcache("art", "art")

    @property
    def manifest(self):
        return self.sfz.get_manifest(self._manifest_vers)

    @property
    def locales(self):
        return self.manifest.locales

    # Bundle-fetching related functions:

    def _fetch_bundle(self, bundle, locale, handle=True):
        versioning = bundle.versionings[locale]
        if versioning['platform'] != 'Default':
            err = f"bundle {bundle.name} has versioning locale {locale} with "
            err += f"platform{versioning['platform']}"
            raise NotImplementedError(err)

        filename = bundle.filename(locale)
        LOGGER.info("Fetch bundle %s", filename)
        platform = "pc" if versioning['platform'] == 'Default' else versioning['platform']
        if self._manifest_vers:
            guid = self.sfz.cfg.vxs.guid(manifest=self._manifest_vers)
        else:
            guid = None
        url = self.sfz.cfg.bundle_url(filename, platform, locale, guid=guid)

        fetcher = lambda: self.ws.fetch(url).content
        res = self.cache.fetch_detailed(os.path.join(platform, locale, filename), fetcher=fetcher)

        ub = UnityBundle(file=res.handle) if handle else None
        if self.validate == "all" or (self.validate == "new" and not res.hit):
            ub = ub or UnityBundle(file=res.handle)
            crc = versioning.get('crc')
            if crc:
                if ub.verify(int(crc)):
                    LOGGER.info("Validate %s OK", filename)
                elif int(crc) == 0:
                    LOGGER.warning("No valid manifest CRC (%s) to validate %s", crc, filename)
                else:
                    LOGGER.warning("Failed validate %s (Manifest CRC: %s)", filename, crc)
            else:
                LOGGER.warning("No manifest CRC to validate %s", filename)
        if not handle and ub:
            ub.close()
            ub = None
        return ub

    def fetch_bundles(self, bundle, locales=None):
        bundle.sanity_check()

        for locale in bundle.versionings:
            if locales and locale not in locales:
                continue
            name = bundle.filename(locale)
            LOGGER.debug("check %s", name)
            try:
                self._fetch_bundle(bundle, locale, handle=False)
            except (NotImplementedError, RuntimeWarning, RuntimeError) as _:
                self.failures.append(name)
                LOGGER.warning("couldn't fetch %s", name)
                traceback.print_exc()

    def fetch_bundle(self, bundle, locale):
        bundle.sanity_check()
        return self._fetch_bundle(bundle, locale)


class BundleScraper(Smeargle):
    def run(self, locales=None, tokens=None):
        LOGGER.info("Fetching asset bundles.")
        manifest = self.manifest
        for bundle in manifest:
            if (not tokens or
                    any(token in bundle.name.split('_') for token in tokens)):
                self.fetch_bundles(bundle, locales)

    def historical(self):
        # 369 to 944 are encrypted.
        versions = [v for v in self.sfz.cfg.vxs.all_versions() if v >= 951]
        all_failures = {}

        for version in versions:
            self._manifest_vers = version
            self.run()
            for fail in self.failures:
                all_failures.setdefault(version, []).append(fail)
            self.failures = []


class CardExtractor(Smeargle):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.extract = None
        self.upload = None

    @property
    def uploader(self):
        self._dragonite = self._dragonite or Dragonite(self.sfz.cfg['dragonite'])
        return self._dragonite

    def _upload(self, localname, remotename, category):
        if {'all', category} & set(self.upload):
            rsp = self.uploader.upload(localname, remotename)
            if not rsp:
                return
            LOGGER.info("uploaded %s", json.dumps(rsp, indent=2))

    def _extract_texture(self, card, relpath, bundle, ldict, ubcache):
        # At the moment, ALL extract options require the tex!
        basepath = relpath.format(ext='png')
        reltexpath = os.path.join('tex', basepath)
        upload_path = os.path.join('art', reltexpath)

        def _writer_fn(fp):
            # Retrieve from cache / open / fetch the Unity Bundle
            bundle_filename = bundle.filename(ldict.locale)
            ub = ubcache.fetch(bundle_filename,
                               lambda: self.fetch_bundle(bundle, ldict.locale))

            # Get the image for this card and write out the data
            img = ub.get(card.texture_file())
            img.save(fp)
            LOGGER.info("wrote %s", reltexpath)

        # Use the data Cache to lazily call _writer_fn
        abstexpath = self.data.put(reltexpath, writer=_writer_fn)
        self._upload(abstexpath, upload_path, 'tex')

        return abstexpath

    def _extract_png(self, relpath, abstexpath):
        relpngpath = os.path.join('png', relpath.format(ext='png'))
        upload_path = os.path.join('art', relpngpath)
        abspngpath = None

        def _crop_fn(fp):
            crop.crop_crush(abstexpath, fp.name)
            LOGGER.info("cropped %s", relpngpath)

        # The cropped png is a requisite for the jpg option, too:
        if self.extract in ('all', 'png', 'jpg'):
            abspngpath = self.data.put(relpngpath, writer=_crop_fn)
            self._upload(abspngpath, upload_path, 'png')

        return abspngpath or self.data.location(relpngpath)

    def _extract_jpg(self, relpath, abspngpath):
        reljpgpath = os.path.join('jpg', relpath.format(ext='jpg'))
        upload_path = os.path.join('art', reljpgpath)

        def _jpg_fn(fp):
            crop.jpg(abspngpath, fp.name)
            LOGGER.info("jpg'd %s", reljpgpath)

        if self.extract in ('all', 'jpg'):
            absjpgpath = self.data.put(reljpgpath, writer=_jpg_fn)
            self._upload(absjpgpath, upload_path, 'jpg')

    def _extract_localized_card(self, card, bundle, ldict, ubcache):
        LOGGER.debug("%s:%s", ldict.locale, str(card))
        common_path = card.cdn_path(self.sdl.get(card.key, {}), ldict, ext='{ext}')
        abstexpath = self._extract_texture(card, common_path, bundle, ldict, ubcache)
        abspngpath = self._extract_png(common_path, abstexpath)
        self._extract_jpg(common_path, abspngpath)

    def _extract_card(self, card, locales, ubcache):
        manifest = self.manifest
        texpath = card.texture_path()
        # NB: SM3 #43 Electivire has two entries; the first is
        # incorrect and the second one is correct. Until this is fixed,
        # the maps must remain lists instead of atoms.
        bundle = manifest.bundles_by_asset(texpath)[-1]

        for locale in bundle.locales:
            if locales and locale not in locales:
                continue
            ldict = self.sfz.localizations[locale]
            self._extract_localized_card(card, bundle, ldict, ubcache)

    def run(self, locales=None, keys=None, upload=None, extract=None):
        LOGGER.info("Art Extraction: loading cards and dictionary")
        ubcache = Ubcache()
        data = self.sfz.definitions
        lastkey = None
        self.upload = upload or []
        self.extract = extract

        for card in sorted(data.cards.values()):
            if keys and card.key not in keys:
                continue

            # For particularly long invocations, close out bundles we're not
            # likely to need again soon. Relies on iteration being sorted.
            if card.key != lastkey:
                LOGGER.info(f"Analyzing {card.key}")
                ubcache.flush()
                lastkey = card.key
            self._extract_card(card, locales, ubcache)

        ubcache.flush()


class RawExtractor(Smeargle):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._ubcache = Ubcache()

    @staticmethod
    def _raw_asset_path(bundle_name, asset_name):
        match = re.match(r"(.*)_(CR[^_]+)$", bundle_name)
        if match:
            return f"{match[2]}/{asset_name}"
        return f"core/{asset_name}"

    def _extract_asset(self, bundle, locale, asset):
        texname = "/".join(asset.split('/')[1:])
        path = self._raw_asset_path(bundle.name, asset) + ".png"
        path = os.path.join("raw", locale, path)

        def _writer(outfile):
            ub = self._ubcache.fetch(bundle.filename(locale),
                                     lambda: self.fetch_bundle(bundle, locale))
            img = ub.get(texname.lower(), strict=False)
            img.save(outfile)
            LOGGER.info("wrote %s", path)

        try:
            self.data.put(path, writer=_writer)
        except KeyError:
            LOGGER.warning("asset %s not found in bundle %s/%s",
                           asset, locale, bundle.name)
        except ValueError:
            LOGGER.warning("bundle %s/%s > asset %s did not extract correctly",
                           locale, bundle.name, asset)

    def run(self, locales=None, tokens=None):
        LOGGER.info("Raw asset extraction")
        man = self.manifest
        known_paths = set()

        # Compute known card texture paths
        for card in self.sfz.definitions.cards.values():
            known_paths.add(card.texture_path())
            if card.holographic:
                known_paths.add(card.foiltex())

        for bun in man:
            # Skip any bundles that don't match our tokens filter
            if (tokens and
                    not any(token.lower() in bun.name.lower().split('_')
                            for token in tokens)):
                continue

            for asset in bun.assets:
                if asset in known_paths:
                    continue
                for locale in bun.locales:
                    if locales and locale not in locales:
                        continue
                    self._extract_asset(bun, locale, asset)
            self._ubcache.flush()


def tokenise(opt):
    tokens = set()
    if opt:
        for item in opt:
            tokens |= set(item.replace(';', ',').split(','))
    return tokens


def build_parser():
    parser = argparse.ArgumentParser(description='Smeargle Art-Fetcher',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--debug',
                        dest='debug',
                        required=False,
                        action="store_true",
                        help="Turn on debugging messages")

    parser.add_argument('--cachedir',
                        dest='cachedir',
                        required=False,
                        action="store",
                        help="Set the bundle cache directory")

    parser.add_argument('--datadir',
                        dest='datadir',
                        required=False,
                        action="store",
                        help="Set the extracted files directory")

    parser.add_argument('--confirm',
                        dest='confirm',
                        required=False,
                        default=False,
                        action="store_true",
                        help="Confirm extraction of all locales and all resources")

    parser.add_argument('--key',
                        dest='key',
                        required=False,
                        action="append",
                        help="Extract data for this expansion, or for bundles, "
                        "select bundles matching this token")

    parser.add_argument('--locale',
                        dest='locale',
                        required=False,
                        action="append",
                        help="Extract data for this locale")

    parser.add_argument('--validate',
                        dest='validate',
                        type=str,
                        choices=["new", "all", "none"],
                        required=False,
                        default="new",
                        help="Choose when to validate Unity3D bundle CRCs")

    parser.add_argument('--manifest',
                        dest='manifest',
                        type=int,
                        required=False,
                        action='store',
                        help="Which manifest version to use")

    subparsers = parser.add_subparsers(
        dest='command',
        required=True,
        help='Smeargle command to execute'
    )

    subparsers.add_parser('showlocales',
                          help="Show availables locales, and then exit")
    # No arguments.

    subparsers.add_parser('assets',
                          help="Extract raw assets not known to be cards")
    # No arguments.

    subparsers.add_parser('bundles',
                          help="Fetch and cache Unity3D bundles")
    # No arguments

    subparser = subparsers.add_parser('cards',
                                      help="Extract card images")
    subparser.add_argument('--extract',
                           choices=['tex', 'png', 'jpg', 'all'],
                           default='tex',
                           required=False,
                           action='store',
                           help="Choose whether to output raw textures, "
                           "crushed and cropped png files, "
                           "compressed and cropped jpg files, "
                           "or all of the above.")
    subparser.add_argument('--upload',
                           required=False,
                           action="append",
                           choices=['tex', 'png', 'jpg', 'all'],
                           help="Choose whether to upload any of the extracted "
                           "files to a remote storage bucket.")

    return parser


_COMMANDS = {
    'assets': RawExtractor,
    'cards': CardExtractor,
    'bundles': BundleScraper,
}


def main():
    kwargs = {}
    logger.standard_setup()
    LOGGER.info("Smeargle, I choose you!")

    parser = build_parser()
    args = parser.parse_args()

    if args.debug:
        logger.adjust_level(logging.DEBUG)
    LOGGER.debug("Debugging ON")

    locales = tokenise(args.locale)
    keys = tokenise(args.key)
    sm = None

    def _get_smeargle(klass=Smeargle):
        return klass(cachedir=args.cachedir, datadir=args.datadir, manifest=args.manifest,
                     validate=args.validate)

    if args.command == 'showlocales':
        sm = Smeargle()
        print("Locales found in manifest:")
        for locale in sm.locales:
            print("\t%s" % locale)
        print("Manifest version: {}".format(sm.manifest.version))
        return

    if not (locales or keys) and not args.confirm:
        sm = _get_smeargle()
        print("No locales or keys specified, but 'confirm' option missing.")
        print("This will download a lot of data, please use --confirm to confirm.")
        print("The currently configured directories are:")
        print("data: {}".format(sm.data.basedir))
        print("cache: {}".format(sm.cache.basedir))
        return

    sm = _get_smeargle(klass=_COMMANDS[args.command])
    if args.command == 'cards':
        if args.upload and not DRAGONITE:
            print("--upload was specified, but the Dragonite uploader module was not loaded.")
            print("Perhaps you are missing the b2sdk module?")
            traceback.print_exception(*DRAGONITE_TRACEBACK)
            return
        kwargs['extract'] = args.extract
        kwargs['upload'] = args.upload or []

    if locales:
        unrecognized = locales - sm.locales
        if unrecognized:
            print("Erroneous locales provided: {}".format(str(unrecognized)))
            return

    sm.run(locales, keys, **kwargs)
    if sm.failures:
        print("failures:")
        for fail in sm.failures:
            print(fail)


if __name__ == '__main__':
    main()
