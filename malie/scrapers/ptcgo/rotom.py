"""
Rotom serves as an archetypes collection class.

Archetypes can be added and will be sorted out into
appropriate subtypes and by set key.
"""

from enum import Enum
import functools
import json
import os
import uuid

import netfleece

from malie.lib import jsondiff
from malie.scrapers.ptcgo import logger
from malie.scrapers.ptcgo.util import slugify
from malie.scrapers.ptcgo.logger import event

LOGGER = logger.get_logger()


def partialclass(classfn, *args, **kwargs):
    """
    Wraps an unbound class method like functools.partial, but for class methods.

    Applies the first argument it receives to the beginning of the argument
    list, to accommodate the `self` parameter.
    """
    @functools.wraps(classfn)
    def inner(self, *iargs, **ikwargs):
        return classfn(self, *args, *iargs, **kwargs, **ikwargs)
    return inner


class GUIDCollisionError(LookupError):
    """Raised when importing definitions and there is a GUID Collision."""


class Cruncher:
    # pylint: disable=too-few-public-methods

    def __init__(self):
        pass

    class Guid:
        @staticmethod
        def test(obj):
            return jsondiff.object_like(obj) and obj.keys() == {'_lo', '_hi'}

        @staticmethod
        def crunch(obj):
            # Google protobuf deserializes 64bit as str; cast to int.
            hi = int(obj['_hi']) << 64
            lo = int(obj['_lo'])
            guid = uuid.UUID(int=(hi | lo))
            return str(guid)

    class Archetype:
        @staticmethod
        def test(obj):
            return (jsondiff.object_like(obj) and
                    obj.keys() >= {'_guid', '_attributes'})

        @staticmethod
        def crunch_attributes(obj):
            attrs = Cruncher.ProtobufList.crunch(obj)
            return {str(k): v for keypair in attrs
                    for (k, v) in keypair.items()}

        @staticmethod
        def crunch(obj):
            data = {
                'guid': Cruncher.Guid.crunch(obj['_guid']),
                'attributes': Cruncher.Archetype.crunch_attributes(obj['_attributes']),
            }

            for k in obj:
                if k not in {'_guid', '_attributes'}:
                    data[k] = Cruncher.crunch(obj[k])
            return data

    class Attribute:
        @staticmethod
        def test(obj):
            if not jsondiff.object_like(obj):
                return False
            if obj.keys() != {'_value', '_name'}:
                return False
            return Cruncher.ProtobufObject.test(obj['_value'])

        @staticmethod
        def crunch(obj):
            # Value should always be a ProtobufObject
            value = Cruncher.ProtobufObject.crunch(obj['_value'])
            return {obj['_name']: value}

    class ProtobufList:
        @staticmethod
        def test(obj):
            if not jsondiff.object_like(obj):
                return False
            if obj.keys() != {'_items', '_size', '_version'}:
                return False
            return jsondiff.array_like(obj['_items'])

        @staticmethod
        def crunch(obj):
            # Protobuf messages sent over the wire use a native array:
            if Cruncher.GenericArray.test(obj):
                return Cruncher.GenericArray.crunch(obj)

            # While MS-NRBF records use a list structure:
            sz = obj['_size']
            return Cruncher.GenericArray.crunch(obj['_items'][:sz])

    class ProtobufObject:
        @staticmethod
        def get_protobuf_type(obj):
            # .NET MS-NRBF Deserialized objects include type as a one-field
            # structure, Deserialized Protobuf objects include it as a flat
            # integer.
            obj_type = obj['_objectType']
            try:
                return obj_type['value__']
            except TypeError:
                return obj_type

        @staticmethod
        def test(obj):
            return jsondiff.object_like(obj) and '_objectType' in obj

        @staticmethod
        def crunch(obj):
            # pylint: disable=too-many-branches

            type_enum = Cruncher.ProtobufObject.get_protobuf_type(obj)

            if type_enum == 1:
                if '_arrayValue' in obj:
                    value = Cruncher.ProtobufList.crunch(obj['_arrayValue'])
                else:
                    value = []
            elif type_enum == 2:
                if '_dictionaryValue' in obj:
                    value = Cruncher.GenericObject.crunch(obj['_dictionaryValue'])
                else:
                    value = {}
            elif type_enum == 3:
                value = obj.get('_stringValue', '')
            elif type_enum == 4:
                value = bool(obj.get('_boolValue', False))
            elif type_enum == 5:
                value = int(obj.get('_intValue', 0))
            elif type_enum == 6:
                value = obj['_floatValue']
            elif type_enum == 7:
                value = Cruncher.Guid.crunch(obj['_guidValue'])
            elif type_enum == 8:
                # (JSON)
                value = json.loads(obj['_stringValue'])
            else:
                raise Exception(f"Unknown dwd protobuf object type {type_enum}")
            return value

    class GenericObject:
        @staticmethod
        def test(obj):
            return jsondiff.object_like(obj)

        @staticmethod
        def crunch(obj):
            return {k:Cruncher.crunch(v) for k, v in obj.items()}

    class GenericArray:
        @staticmethod
        def test(obj):
            return jsondiff.array_like(obj)

        @staticmethod
        def crunch(obj):
            return [Cruncher.crunch(v) for v in obj]

    @classmethod
    def crunch(cls, obj):
        crunchers = [cls.Guid, cls.Archetype, cls.Attribute, cls.ProtobufList,
                     cls.ProtobufObject, cls.GenericObject, cls.GenericArray]
        for cruncher in crunchers:
            if cruncher.test(obj):
                return cruncher.crunch(obj)
        return obj


class Archetype:
    """
    Archetype represents one game object in Pokemon TCG Online.

    Archetypes are mostly cards, but also include avatar clothing, products,
    and a few miscellanous things.
    """

    class Attribute(Enum):
        """
        Attribute is an enumerator class that maps attribute numbers to names.

        Pokemon TCG Online uses numerical properties to send to the game client.
        This class maps them back to human readable names.
        """
        # pylint: disable=bad-whitespace

        # Global
        assetName          =  10020
        hidden             =  10090
        name               =  10140
        setSortOrder       =  10190
        rarity             = 200550
        archKey            = 200580
        dateAdded          = 202220
        ## Products
        productDescription =  10060
        productType        =  10540
        ### Packs
        cardsPerPack       =  10200
        packLocale         =  10300
        packOdds           = 202250
        ### Bundles
        bundleType         = 201507
        ### DeckBox
        deckBoxTypeA       = 201250
        deckBoxTypeB       = 201260
        deckBoxColorA      = 201930
        deckBoxColorB      = 201940
        secondaryTypeName  = 201941
        # Card Properties
        ## Common Card Properties
        guid               =  10000
        cardType           = 200300
        rulesText          = 200310
        teamAffinity       = 200360
        leagueCard         = 200400
        _unusedBoolA       = 200520
        subRarity          = 200560
        foilEffect         = 200610
        foilMask           = 200620
        shortName          = 200630
        abilities          = 200740
        cardNo             = 200780
        cardNoText         = 200790
        issue              = 200871
        fullArt            = 201000
        _unusedTimeA       = 201620
        originalPrintID    = 201710
        containedInDecks   = 201730
        foilIntensity      = 202080
        legalityDate       = 202110
        tags               = 202200
        ## Pokemon & LegendHalf Card Properties
        familyID           = 200260
        previousEvolution  = 200280
        burnAmount         = 200430
        hp                 = 200490
        stage              = 200540
        types              = 200570
        weaknesses         = 200590
        resistance         = 200600
        evolvesFrom        = 200640
        resistanceOp       = 200650
        weaknessOp         = 200660
        retreatCost        = 200800
        weaknessAmt        = 200820
        resistanceAmt      = 200830
        exPokemon          = 201010
        legendPokemon      = 201030
        numPokeTools       = 201610
        fossilBase         = 201750
        gxAbilities        = 202120
        ## Trainer Card Properties
        pokeToolText       = 200200
        trainerType        = 200270
        _unusedBoolB       = 201970
        ## Energy Card Properties
        specialEnergy      = 200970
        energyProvided     = 201040

        @classmethod
        def attr_name(cls, key: int):
            """
            Non-strict name resolution for a numeric key. Returns key as-is when not found.
            For strict resolution, use cls(value).name directly.
            """
            try:
                return cls(key).name
            except ValueError:
                return key

    class Issue(Enum):
        """Enum representing the sort order of card issue types."""
        # pylint: disable=invalid-name,bad-whitespace
        std = 0
        ph  = 1
        pcd = 2
        alt = 3
        yaa = 4
        op  = 5
        ref = 6

    def __init__(self, key, obj: dict):
        """Initialize an archetype from a key and a 'crunched' archetype object."""
        self._obj = obj
        self._key = key
        self._guid = obj['guid']
        self._attributes = {int(k): v for k, v in obj['attributes'].items()}

    def __str__(self):
        if self.attr('cardType'):
            return "{} #{} {} [{}]".format(self.key,
                                           self.attr('cardNoText') or self.attr('cardNo'),
                                           self.attr('shortName'),
                                           self.attr('issue'))
        if self.attr('productType'):
            return "{} {}".format(self.key,
                                  self.attr('name'))
        # misc items
        return "{} {}".format(self.key, self.attr('name'))

    def search_str(self):
        if self.attr('cardType'):
            name = self.attr('name')
            num = self.attr('cardNoText') or self.attr('cardNo')
            key = self.key
            abbr = "{abbr}"
            issue = self.attr('issue')
            return f"{name} {num} {key} {abbr} {issue}".lower()
        return None

    def __repr__(self):
        return "{}(\"{}\", {})".format(
            type(self).__name__,
            self.key,
            repr(self._obj))

    def __eq__(self, other):
        return (self.key == other.key and
                self.obj == other.obj)

    def __lt__(self, other):
        return self.sortkey() < other.sortkey()

    def __gt__(self, other):
        return self.sortkey() > other.sortkey()

    def __le__(self, other):
        return self.sortkey() <= other.sortkey()

    def __ge__(self, other):
        return self.sortkey() >= other.sortkey()

    def _resolve_key(self, key):
        keyval = None

        # Try as an integer first
        try:
            keyval = self.Attribute(key).value
        except ValueError:
            pass

        # Try as a string value next
        if keyval is None:
            keyval = self.Attribute[key].value

        return keyval

    def __getitem__(self, key):
        return self._attributes[self._resolve_key(key)]

    def __setitem__(self, key, value):
        resolved_key = self._resolve_key(key)
        self._attributes[resolved_key] = value

    @staticmethod
    def from_raw(key, raw: dict):
        """Initialize an archetype from a key and a raw, 'un-crunched' archetype object."""
        obj = Cruncher.crunch(raw)
        return Archetype(key, obj)

    @property
    def guid(self):
        """Returns the archetype's GUID."""
        return self._guid

    @property
    def key(self):
        """Returns the archetype's associated key."""
        return self._key

    @property
    def obj(self):
        """Returns the minified, json-like archetype."""
        return self._obj

    @property
    def raw_attributes(self):
        """Returns raw, untranslated attributes dict."""
        return self._attributes

    @property
    def attributes(self):
        return {self.Attribute.attr_name(k): v for k, v in
                self._attributes.items()}

    def attr(self, key, default=None):
        """
        Gets an architecture attribute.

        Key can be either a number (10000),
        a string of that numeral ("10000"),
        or the logical name if known ("guid").
        """
        # Try to force '10000' as 10000
        try:
            key = int(key)
        except ValueError:
            pass

        # Fetch by numerical key
        if isinstance(key, int):
            try:
                keyval = self.Attribute(key).value
            except ValueError:
                keyval = key
            return self._attributes.get(keyval, default)

        # Fetch by attribute name (or string literal)
        try:
            keyval = self.Attribute[key].value
        except KeyError:
            keyval = key
        return self._attributes.get(keyval, default)

    def compare(self, other):
        """
        Compares this Archetype against another one.

        Returns two json-like objects, which are representations of
        self and other, respectively. These json-like objects represent
        only the unique values inherent to each Archetype.
        """
        return jsondiff.json_compare({"key": self.key,
                                      **self.attributes},
                                     {"key": other.key,
                                      **other.attributes})

    def texture_file(self):
        """Returns just the texture file for this arch's texture."""
        return self.attr('assetName') or "{:03d}".format(int(self.attr('cardNo')))

    def texture_path(self):
        """Returns the asset path for this arch's texture."""
        texfile = self.texture_file()
        return f"{self.attr('archKey')}/{texfile}"

    @property
    def holographic(self):
        effect = self.attr('foilEffect') not in (None, 'None')
        mask = self.attr('foilMask') not in (None, 'None')
        return effect and mask

    def foiltex(self):
        """Returns the asset name for this arch's foil overlay."""

        if not self.holographic:
            return None

        if self.attr('foilEffect') == 'Cracked_Ice':
            issue = 'pcd'
        elif self.attr('foilMask') == 'Reverse':
            issue = 'ph'
        else:
            issue = 'std'

        return f"{self['archKey']}_wp_{issue}_Foil2/{self.texture_file()}"

    @property
    def cardNo(self):
        """
        cardNo returns either the number or the textual number.
        Some cards have text numbers, like "SM190".

        WARNING: This data can contain localization tokens.
        """
        if self.attr('cardNoText'):
            return self.attr('cardNoText').split("/")[0].replace(" ", "_")
        return "{:03d}".format(self.attr('cardNo'))

    def image_name(self, locdata):
        """Returns the extracted image name for this arch."""
        card_no = locdata.detokenize(self.cardNo, strict=True)
        cardname = locdata.detokenize(self.attr('name'), strict=True)
        cardname = slugify(cardname, allow_unicode=True, translate_latin=True)
        name = f"{locdata.locale}-{self.key}-{card_no}-{cardname}"

        # If this card uses the "default" texture, name it the basename.
        aname = self.attr('assetName', '')
        if not aname:
            return name

        # Amend this card with an issue tag.
        issue = self.attr('issue')
        name += f"-{issue}"

        # Issue isn't guaranteed to be unique (There may be several cards of this issue),
        # so use the texture name as a disambiguator if necessary.
        # In some cases, it's a known format that holds no disambiguating information.

        # Remove this card collection number from the texture name.
        # This will leave us with "op" or "xy" in most cases,
        # a locale code (en, it, de) in some others, and
        # "gold" and "silver" in a scant few more.
        num = "{:03d}".format(self.attr('cardNo'))
        disambig = aname.replace(num, "").replace("_", "")

        # This is an OP card whose texture is simply 123op.
        if issue == 'op' and aname == f"{num}op":
            disambig = ""

        # This is an alt/pcd/yaa/ref/std/ph using a standard alt-texture:
        if aname == f"{num}xy":
            disambig = ""

        # We might have a locale (en/it/de) or a coloring (silver/gold),
        # or simply just a disambiguator like "ya" or "a"
        if disambig:
            name += f"-{disambig}"

        return name

    def cdn_path(self, setinfo, unown, ext='png'):
        """cdn_path determines the output folder/name."""
        # Replace '-' with '_' to make it clear that e.g. TK_Bisharp is one
        # name when used in contexts like "TK5A-TK_Bisharp"

        # FIXME: Use converged set data instead of SDL access.
        extkey = setinfo.get('externalId', '').replace("-", "_")

        locale = unown.locale
        series = setinfo.get('block', 'NONE')
        if (not extkey) or (extkey == self.key):
            folder = self.key
        else:
            folder = f"{self.key}-{extkey}"

        basename = self.image_name(unown)
        outpath = f"{locale}/{series}/{folder}/{basename}.{ext}"
        return outpath

    def sortkey(self):
        return (self.attr('setSortOrder'),
                self.attr('cardNo'),
                self.Issue[self.attr('issue')].value)


class RotomSlice:
    """
    RotomSlice serves as an Archetype collections class.

    Archetypes are sorted both by key or by GUID.
    This object behaves like a dict of guid:archetype mappings.
    """

    def __init__(self, arches):
        self._archetypes = {}
        self._keyed = {}
        self._touched = {}
        self._prodtypes = set()
        self._cardtypes = set()
        self._batch(arches, initial=True)

    def __len__(self):
        return len(self._archetypes)

    def __getitem__(self, guid):
        return self._archetypes[guid]

    def __delitem__(self, guid):
        item = self[guid]
        self._touched[item.key] = True
        del self._keyed[item.key][guid]
        del self._archetypes[guid]

    def __iter__(self):
        return iter(self._archetypes)

    def __contains__(self, guid):
        return guid in self._archetypes

    def values(self):
        """Iterator over the archetypes themselves, like a dict offers."""
        return self._archetypes.values()

    @property
    def arch_keys(self):
        return self._keyed.keys()

    @staticmethod
    def _define_meta_properties(getter_method, enumeration):
        """
        Modifies the class with new properties specified in `enumeration`.

        Creates a series of meta-properties defined for the RotomSlice class
        named for each string in `enumeration`. Each newly created property
        calls getter_method (which must be a method of the RotomSlice class)
        with the string from `enumeration`.
        """
        for atom in enumeration:
            setattr(RotomSlice, atom,
                    property(partialclass(getter_method, atom)))

    def _compute_meta(self):
        """
        Calculates and defines the meta-properties for the class.

        The .KEY properties will be added based on arch keys in the object.
        The .CARDTYPE properties will be added based on cards in the object.
        The .PRODTYPE properties will be added based on products in the object.
        """
        # Note: theoretically these sub-types could clash with existing
        # attribute names, but we can at least assert they don't clash
        # amongst each other.
        assert not self._prodtypes & self._cardtypes
        assert not self._prodtypes & self._keyed.keys()
        assert not self._keyed.keys() & self._cardtypes
        self._define_meta_properties(type(self).get_products, self._prodtypes)
        self._define_meta_properties(type(self).get_cards, self._cardtypes)
        self._define_meta_properties(type(self).arch_key, self._keyed.keys())

    def _add(self, arch: Archetype, initial=False, meta=True):
        """
        Adds a single Archetype to the Slice.

        If initial is True, raise an exception on GUID collision, do not log any
        "NEW" or "MODIFIED" events, and do not mark any decks as dirty.
        If initial is False, GUID collisions will update that Architecture.
        NEW and MODIFIED events will be logged, and decks will be marked dirty.
        The default is False.

        If meta is False, do not recompute meta-properties after addition.
        The default is True.
        """
        if arch.guid in self:
            if initial:
                raise GUIDCollisionError(f"Duplicate GUID detected: {arch.guid}")
            if arch == self[arch.guid]:
                return
            diff_old, diff_new = self[arch.guid].compare(arch)
            differences = jsondiff.DotDiff.from_json(diff_old, diff_new)
            event("ARCH.MODIFIED", "%s/%s %s\n%s",
                  arch.key, arch.guid, str(arch),
                  str(differences))
        elif not initial:
            event("ARCH.NEW", "%s/%s %s", arch.key, arch.guid, str(arch))

        # Add Archetype
        self._archetypes[arch.guid] = arch

        # Add Archetype to Keyed Cache
        self._keyed.setdefault(arch.key, {})[arch.guid] = arch
        if not initial:
            self._touched[arch.key] = True

        # Add productType/cardType as appropriate to set of known types
        if arch.attr('productType'):
            self._prodtypes.add(arch.attr('productType'))
        if arch.attr('cardType'):
            self._cardtypes.add(arch.attr('cardType'))

        if meta:
            self._compute_meta()

    def _batch(self, arches: [Archetype], initial=False):
        """Given a list of Archetypes, add them to the Slice."""
        for arch in arches:
            self._add(arch, initial=initial, meta=False)
        self._compute_meta()

    def product_types(self):
        """Return a list of strings for the types of products in this slice."""
        return set((x.attr('productType') for x in self.products.values()))

    def card_types(self):
        """Return a list of strings for the types of cards in this slice."""
        return set((x.attr('cardType') for x in self.cards.values()))

    # Research Methods

    def attrib_enum(self):
        """Return a set of all known attribute keys."""
        akeys = set()
        for arch in self.values():
            akeys.update(arch.raw_attributes.keys())
        return akeys

    def value_enum(self, key):
        """Return a set of all known attribute values."""
        vals = set()
        for arch in self.values():
            val = arch.attr(key)
            try:
                vals.add(val)
            except TypeError:
                vals.add(repr(val))
        return vals

    def mysteries(self):
        """Return a set of all unique un-named properties."""
        unknown = set()
        for attribute in self.attrib_enum():
            value = int(attribute)
            try:
                _ = Archetype.Attribute(value)
            except ValueError:
                unknown.add(value)
        return unknown

    def query(self, kvs, ormatch=False):
        """
        Return a RotomSlice of Archetypes whose attributes meet certain criteria.

        kvs is a dict of {key: value} pairs.
        Archetypes are filtered out if they do not have a matching 'key'.
        If 'value' isn't None, The Archetype must match both 'key' and 'value'.

        Examples:
        {20000: None}      - Matches for Archetypes with attribute 20000 set.
        {20280: "Pikachu"} - Matches for Archetypes with attribute 20280 set
                             to "Pikachu".
        """
        def _filterfunc(arch):
            for k, v in kvs.items():
                if (k in arch.raw_attributes and
                        ((v and arch.attr(k) == v) or (v is None))):
                    if ormatch:
                        return True
                elif not ormatch:
                    return False
            # If ormatch was true, we found no matches.
            # If it was false, we found no disqualifiers.
            return not ormatch
        return self.filter(_filterfunc)

    # Slicing Methods

    def filter(self, func):
        """Return a RotomSlice of Archetypes based on func."""
        return RotomSlice(filter(func, self.values()))

    @property
    def cards(self):
        """Return a RotomSlice containing only Card Archetypes."""
        return self.filter(lambda a: a.attr('cardType') is not None)

    def get_cards(self, category):
        """Return a RotomSlice containing only Cards of a certain type."""
        return self.filter(lambda a: a.attr('cardType') == category)

    @property
    def products(self):
        """Return a RotomSlice containing only Product Archetypes."""
        return self.filter(lambda a: a.attr('productType') is not None)

    def get_products(self, category):
        """Return a RotomSlice containing only Products of a certain type."""
        return self.filter(lambda a: a.attr('productType') == category)

    @property
    def misc(self):
        """Return a RotomSlice containing only the Miscellaneous Archetypes."""
        def _misc(arch: Archetype):
            return (arch.attr('productType') is None and
                    arch.attr('cardType') is None)
        return self.filter(_misc)

    def arch_key(self, key):
        """Return a RotomSlice containing Archetypes under a specific key."""
        return RotomSlice(self._keyed[key].values())


class Rotom(RotomSlice):
    """
    Rotom is a collection of Archetypes, derived from RotomSlice.

    Rotom features an overridden constructor that takes a filename,
    as well as methods to save or load definitions from file.

    Lastly, Rotom offers a bulk import/update from ArchetypesFound messages.
    """

    def __init__(self, filename=None):
        self.filename = filename
        self._checksums = {}
        super().__init__([])
        if filename:
            try:
                self.load(filename)
            except FileNotFoundError:
                # In some cases, we don't find the sub-decks; we need to re-raise
                if os.path.exists(filename):
                    raise

    def load_deck(self, arch_key, deckfile):
        """Load an individual deck from file."""
        LOGGER.debug("load_deck file=%s", deckfile)
        with open(deckfile, 'rb') as f:
            j = json.load(f)
            self._batch([Archetype(arch_key, arch) for arch in j],
                        initial=True)

    def load(self, filename=None):
        """Loads either a monolithic or split-deck load, based on the file."""
        LOGGER.debug("load %s", filename)
        self.filename = filename or self.filename
        with open(self.filename, 'rb') as f:
            j = json.load(f)
        tgt_dir = os.path.dirname(filename)
        old_dir = None
        if tgt_dir:
            old_dir = os.getcwd()
            os.chdir(tgt_dir)
        for arch_key, entry in j.items():
            self._checksums[arch_key] = entry['checksum']
            if 'archetypes' in entry:
                self._batch([Archetype(arch_key, arch) for
                             arch in entry['archetypes']],
                            initial=True)
            else:
                self.load_deck(arch_key, entry['deck'])
        if tgt_dir and old_dir:
            os.chdir(old_dir)

    def save_monolithic(self, filename=None):
        """Performs a monolithic-style save. This can be a bit slow."""
        self.filename = filename or self.filename
        with open(self.filename, 'w') as f:
            j = {}
            for arch_key, checksum in self._checksums.items():
                j[arch_key] = {
                    'checksum': checksum,
                    'archetypes': [arch.obj for arch in
                                   self.arch_key(arch_key).values()],
                }
            json.dump(j, f)

    def save_deck(self, arch_key, filename):
        """Saves a single deck to file."""
        with open(filename, 'w') as f:
            arches = [arch.obj for arch in self.arch_key(arch_key).values()]
            json.dump(arches, f)
        self._touched[arch_key] = False

    def save_decks(self, filename=None, dirname="decks"):
        """Save all dirtied decks to their respective files."""
        self.filename = filename or self.filename
        dname = os.path.dirname(self.filename)
        subdir = os.path.join(dname, dirname)
        os.makedirs(subdir, exist_ok=True)
        j = {}
        for arch_key, checksum in self._checksums.items():
            deck = "{}.json".format(arch_key)
            reldeckfile = os.path.join(dirname, deck)
            absdeckfile = os.path.join(subdir, deck)
            j[arch_key] = {
                'checksum': checksum,
                'deck': reldeckfile
            }
            if self._touched.get(arch_key) or not os.path.exists(absdeckfile):
                self.save_deck(arch_key, absdeckfile)
        with open(self.filename, 'w') as f:
            json.dump(j, f)

    def add_archetypes(self, arch_key, checksum, archlist):
        """Bulk import archetypes using the key, checksum, and a list of (raw) archetypes"""

        arches = [Archetype(arch_key, arch) for arch in archlist]
        if arch_key in self._checksums:
            if self._checksums[arch_key] == checksum:
                LOGGER.debug("Arch Key %s checksum is unchanged.", arch_key)
            else:
                event("KEY.MODIFIED", "%s updated from %s to %s",
                      arch_key, self._checksums[arch_key], checksum)
            old_guids = set(self.arch_key(arch_key))
            new_guids = set(arch.guid for arch in arches)
            for stale in old_guids - new_guids:
                event("ARCH.STALE", "%s/%s %s",
                      arch_key, stale, str(self[stale]))
                del self[stale]
        else:
            event("KEY.NEW", "%s", arch_key)
        self._checksums[arch_key] = checksum
        self._batch(arches)

    def add_archetypes_ptcgo(self, archetypes_found: dict):
        """Bulk import archetypes from a PTCGO ArchetypesFound message."""

        processed = Cruncher.crunch(archetypes_found)
        arch_key = archetypes_found['_key']
        checksum = archetypes_found['_checksum']
        return self.add_archetypes(arch_key, checksum, processed['_archetypes'])

    def load_from_msnrbf(self, filename):
        """
        Load definitions from a file in the PTCGO installation folder;
        .../StreamingAssets/tcgo-gateway.direwolfdigital.com
        """
        with open(filename, 'rb') as infile:
            rawmsgs = netfleece.parseloop(infile, expand=True, backfill=True,
                                          crunch=True, root=True)
            for msg in rawmsgs:
                self.add_archetypes_ptcgo(msg)

    def checksum(self, arch_key):
        """Get the server checksum corresponding to the given arch_key."""
        return self._checksums[arch_key]
