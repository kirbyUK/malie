#!/usr/bin/python3

import json
import time
import sys

from malie.lib import jsondiff


class Bundle:
    def __init__(self, obj):
        self._raw = obj
        self._assets = tuple(a['name'] for a in obj['assets'])
        self._name = obj['name']
        self._versionings = None

    def __len__(self):
        return len(self.assets)

    def __iter__(self):
        return iter(self.assets)

    def __contains__(self, value):
        return value in self.assets

    def __eq__(self, other: 'Bundle'):
        return (type(self) == type(other) and
                self._raw == other.dump)

    @property
    def assets(self):
        return self._assets

    @property
    def name(self):
        return self._name

    @property
    def encrypted(self):
        return self._raw['encrypted']

    @property
    def timesensitive(self):
        return self._raw.get('timesensitive')

    @property
    def precached(self):
        return self._raw.get('precached', [])

    @property
    def versionings(self):
        if not self._versionings:
            tmp = {v['locale']: v for v in self._raw['versionings']}
            self._versionings = tmp
        return self._versionings

    @property
    def locales(self):
        return set({v['locale'] for v in self._raw['versionings']})

    @property
    def dump(self):
        return json.loads(json.dumps(self._raw))

    def filename(self, locale):
        vers = self.versionings[locale]
        return "{}_{}_{}.unity3d".format(locale,
                                         self._name,
                                         vers['version'])

    def has_IV(self, IV):
        for vers in self._raw['versionings']:
            if vers.get('IV') == IV:
                return True
        return False

    def sanity_check(self):
        if self.encrypted:
            raise NotImplementedError(f"bundle {self.name} is encrypted")

        if self.timesensitive:
            btime = self.timesensitive / 1000
            if time.time() < btime:
                timestr = time.ctime(btime)
                raise RuntimeWarning(f"bundle {self.name} is timesensitive "
                                     f"and future-dated: {timestr}")

    # Comparison Methods

    def _compare_versionings(self, other: 'Bundle', squelch_hollow=True, file=sys.stdout):
        # Compare Versionings (keyed by locale)
        lhvers = other.versionings.copy()
        rhvers = self.versionings.copy()
        if squelch_hollow:
            # If the CRC is the same, suppress the diff output.
            for locale in other.versionings:
                if (locale in rhvers and
                        lhvers[locale].get('crc') == rhvers[locale].get('crc')):
                    del lhvers[locale]
                    del rhvers[locale]
        ldiff, rdiff = jsondiff.json_compare(lhvers, rhvers)
        jdd = jsondiff.DotDiff.from_json(ldiff, rdiff)
        jdd.print(indent=7, file=file)

    def _compare_precached(self, other: 'Bundle') -> bool:
        if self.precached == other.precached:
            # Identical; no need to show the generic diff.
            return False

        if len(self.precached) != len(other.precached):
            # Something substantial changed, show the generic diff.
            return True

        for selfpc, otherpc in zip(self.precached, other.precached):
            if selfpc == otherpc:
                continue
            elif (selfpc['alt_platform'] == otherpc['alt_platform'] and
                  selfpc['alt_locale'] == otherpc['alt_locale'] and
                  selfpc['alt_version'] == self.versionings[selfpc['alt_locale']]['version'] and
                  otherpc['alt_version'] == other.versionings[otherpc['alt_locale']]['version']):
                # Only the version changed, and the versions before and
                # after correlate directly to the recorded locale versions
                # before and after. Nothing too interesting to show, then.
                continue
            else:
                # Something else changed, show the generic diff.
                return True
        return False

    def compare(self, other: 'Bundle', indent=8, squelch_hollow=True, file=sys.stdout):
        def print_section(title, assets):
            if not assets:
                return
            print("{:{:d}s}# {:s}".format("", indent, title), file=file)
            for name in assets:
                print("{:{:d}s}{:s}".format("", indent + 4, name), file=file)

        # Begin compare()
        scopy, ocopy = self.dump, other.dump

        # Compare Assets
        lhs = set(other.assets)
        rhs = set(self.assets)
        print_section("New Assets", sorted(list(rhs - lhs)))
        print_section("Removed Assets", sorted(list(lhs - rhs)))
        del scopy['assets']
        del ocopy['assets']

        # Versionings
        self._compare_versionings(other, squelch_hollow, file=file)
        del scopy['versionings']
        del ocopy['versionings']

        # Precached
        if not self._compare_precached(other):
            if 'precached' in scopy:
                del scopy['precached']
            if 'precached' in ocopy:
                del ocopy['precached']

        # Compare anything remaining, possibly including precached:
        ldiff, rdiff = jsondiff.json_compare(ocopy, scopy)
        jdd = jsondiff.DotDiff.from_json(ldiff, rdiff)
        jdd.print(indent=7, file=file)


class Manifest:
    def __init__(self, raw, version=None):
        # Asset-to-[bundle.name]
        self.assets = {}
        # Bundle.name-to-bundle
        self.bundles = {}
        self._raw = raw
        self._populate_maps()
        # names: pre-computed list of bundle names.
        self.names = frozenset(x['name'] for x in self._raw['bundles'])
        # Manifests loaded from file don't necessarily have their version information, do our best:
        self.version = version if version else -1

    @staticmethod
    def from_file(filename):
        with open(filename, "rb") as f:
            raw = json.load(f)
        return Manifest(raw)

    def _populate_maps(self):
        for bundle_json in self._raw['bundles']:
            bundle = Bundle(bundle_json)
            self.bundles[bundle.name] = bundle
            for asset_name in bundle:
                amap = self.assets.setdefault(asset_name, [])
                amap.append(bundle.name)

    def __len__(self):
        return len(self.bundles)

    def __contains__(self, value):
        return value in self.bundles

    def __getitem__(self, key):
        return self.bundles[key]

    def __iter__(self):
        return iter(self.bundles.values())

    def _print_asset_names(self, bname, file=sys.stdout):
        for assetname in self[bname].assets:
            print("        %s" % assetname, file=file)

    @property
    def locales(self):
        locs = set()
        for bundle in self:
            locs = locs | bundle.locales
        return locs

    # Search / Selection Methods

    def filter(self, fn):
        return filter(fn, self.bundles.items())

    def by_name(self, name, fuzzy=False):
        _fun = lambda kv: (((not fuzzy) and (kv[0] == name)) or
                           (fuzzy and (name in kv[0])))
        return self.filter(_fun)

    def by_IV(self, IV):
        _fun = lambda kv: kv[1].has_IV(IV)
        return self.filter(_fun)

    def by_asset(self, asset):
        """Return an iterable of name:bundle pairs that contain the named asset."""
        _fun = lambda kv: asset in kv[1]
        return self.filter(_fun)

    def bundles_by_asset(self, aname):
        """Return a list of bundles that contain the named asset."""
        return [self[bname] for bname in self.assets[aname]]

    # Comparison Methods

    def new_since(self, other: 'Manifest'):
        return self.names - other.names

    def removed_since(self, other: 'Manifest'):
        return other.names - self.names

    def common_with(self, other: 'Manifest'):
        return other.names & self.names

    def modified_since(self, other: 'Manifest'):
        modified = set()
        for name in self.common_with(other):
            if self[name] != other[name]:
                modified.add(name)
        return modified

    def _print_section(self, title, nameslist, print_assets=False, file=sys.stdout):
        if nameslist:
            print("# %s" % title, file=file)
            for name in nameslist:
                print("    %s" % name, file=file)
                if print_assets:
                    self._print_asset_names(name, file=file)
            print("", file=file)

    def compare(self, other: 'Manifest', print_assets=False, file=sys.stdout):
        self._print_section("New", sorted(self.new_since(other)),
                            print_assets, file=file)
        other._print_section("Removed", sorted(self.removed_since(other)),
                             print_assets, file=file)
        # Modified Bundles
        tmplist = sorted(self.modified_since(other))
        if tmplist:
            print("# Modified", file=file)
            for name in tmplist:
                print("    %s" % name, file=file)
                self[name].compare(other[name], file=file)
        print("", file=file)
