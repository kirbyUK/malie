import json

from malie.lib import resource
from malie.scrapers.ptcgo.kecleon import Kecleon
from malie.scrapers.ptcgo.rotom import Rotom
from malie.scrapers.ptcgo.unown import UnownZukan
from malie.scrapers.ptcgo.porygon import Manifest
from malie.scrapers.ptcgo.smeargle import CardExtractor


SAFARI_ZONE = None


class PTCGODirs:
    def __init__(self, root):
        self.unity = None
        self.tpci = None
        self.ptcgo = None
        self.data = None
        self.assets = None
        self.locdata = None
        self.dwddata = None
        self.root = root
        self.gen_folders(root)

    def gen_folders(self, root=None):
        self.root = root or self.root
        self.gen_appdata_folders()
        self.gen_install_folders()

    def gen_appdata_folders(self, llow=None):
        if not llow:
            llow = f"{self.root}/AppData/LocalLow"
        self.unity = f"{llow}/Unity/WebPlayer/Cache/The Pokémon Company International_Pokemon Trading Card Game Online"
        self.tpci = f"{llow}/The Pokémon Company International/Pokemon Trading Card Game Online"

    def gen_install_folders(self, install_folder=None):
        # Installation folders
        if not install_folder and not self.ptcgo:
            datadir = f"{self.root}/Application Data"
            self.ptcgo = f"{datadir}/Pokémon Trading Card Game Online/PokemonTradingCardGameOnline"
        else:
            self.ptcgo = install_folder
        self.data = f"{self.ptcgo}/Pokemon Trading Card Game Online_Data"
        self.assets = f"{self.data}/StreamingAssets"
        self.locdata = f"{self.assets}/en_US"
        self.dwddata = f"{self.assets}/tcgo-gateway.direwolfdigital.com"


class SafariZone:
    def __init__(self, kcfg: Kecleon = None, definitions: Rotom = None,
                 localizations: UnownZukan = None,
                 art_extractor: CardExtractor = None):
        self._kecleon = kcfg or Kecleon()
        self._definitions = definitions
        self._localizations = localizations
        self._art_extractor = art_extractor
        self._sdm = None

    def boot(self):
        if not self._kecleon.booted:
            self._kecleon.boot()

    @property
    def cfg(self) -> Kecleon:
        return self._kecleon

    @property
    def definitions(self) -> Rotom:
        if not self._definitions:
            fname = self.cfg.cache.base.location('rotom.json')
            self._definitions = Rotom(fname)
        return self._definitions

    @property
    def localizations(self) -> UnownZukan:
        if not self._localizations:
            fname = self.cfg.cache.base.location('unown.json')
            self._localizations = UnownZukan(fname)
        return self._localizations

    @property
    def manifest(self) -> Manifest:
        return self.get_manifest()

    def get_manifest(self, version=None) -> Manifest:
        """Determine the latest version and return the corresponding manifest."""
        return self.cfg.fetch_manifest(version)

    @property
    def art_extractor(self) -> CardExtractor:
        if not self._art_extractor:
            art = CardExtractor()
            self._art_extractor = art
        return self._art_extractor

    @property
    def sdm(self) -> dict:
        # FIXME: Remove this hacky function (get_sdl/sdm/get_sdm)
        if not self._sdm:
            fname = resource.get_resource('SetDataMap.json', cache=self._kecleon.cache.metamon)
            with open(fname, "r") as fh:
                self._sdm = json.load(fh)
        return self._sdm


def get_safari_zone():
    global SAFARI_ZONE
    if not SAFARI_ZONE:
        SAFARI_ZONE = SafariZone()
    return SAFARI_ZONE
