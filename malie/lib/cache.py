#!/usr/bin/env python3

from collections import namedtuple
import os
import xdg.BaseDirectory
import shutil

class Cache:
    FetchResult = namedtuple('FetchResult', ['filepath', 'handle', 'hit'])

    def __init__(self, name, basedir=None, data=False):
        self.name = name
        if not basedir:
            if data:
                basedir = xdg.BaseDirectory.save_data_path(name)
            else:
                basedir = xdg.BaseDirectory.save_cache_path(name)

        if not os.path.exists(basedir):
            os.makedirs(basedir, exist_ok=True)
        self.basedir = basedir

    def location(self, path):
        return os.path.join(self.basedir, path)

    def exists(self, path):
        return os.path.exists(self.location(path))

    def drop(self, path, rmdir=False):
        abspath = self.location(path)
        try:
            os.remove(abspath)
        except IsADirectoryError:
            if rmdir:
                shutil.rmtree(abspath)
            else:
                raise

    def fetch_lazy(self, path, *, force=False, fetcher=None, writer=None):
        """
        fetch_lazy does the bare minimum of fetching from the cache.

        :param force: If true, ignore the cache.

        Returns (filepath, None) if the file exists in cache.
        Returns (filepath, handle) if the file did not exist in cache,
                                   and it had to write a data buffer.
        """
        if not bool(fetcher) ^ bool(writer):
            raise ValueError("Must specify exactly one of fetcher or writer callbacks")

        if not force:
            if self.exists(path):
                return Cache.FetchResult(self.location(path), None, True)

        self.mkdir(os.path.dirname(path))
        resolved_path = self.location(path)
        try:
            fh = open(resolved_path, "wb+")
            if writer:
                writer(fh)
            else:
                data = fetcher()
                fh.write(data)
            fh.flush()
            fh.seek(0)
            return Cache.FetchResult(resolved_path, fh, False)
        except:
            try:
                self.drop(path)
            except FileNotFoundError:
                pass
            raise

    def fetch_detailed(self, *args, **kwargs):
        """Fetch an item from the cache, returning a FetchResult with an open handle."""
        lazy = self.fetch_lazy(*args, **kwargs)
        if not lazy.handle:
            handle = open(lazy.filepath, "rb")
            return Cache.FetchResult(lazy.filepath, handle, lazy.hit)
        return lazy

    def fetch(self, *args, **kwargs):
        """Fetch an item from the cache, or the fetcher lambda otherwise."""
        return self.fetch_detailed(*args, **kwargs).handle

    def put(self, *args, **kwargs):
        """If filename is not in the cache, fetch it and put it into the cache."""
        lazy = self.fetch_lazy(*args, **kwargs)
        if lazy.handle:
            lazy.handle.close()
        return lazy.filepath

    def subcache(self, name, subpath=None):
        """Create a cache relative to this one with a given subpath."""
        subpath = subpath if subpath else name
        return Cache(".".join([self.name, name]),
                     os.path.join(self.basedir, subpath))

    def mkdir(self, subpath):
        if not self.exists(subpath):
            os.makedirs(self.location(subpath), exist_ok=True)

    def write(self, path, data):
        def _writer(fh):
            fh.write(data)
        return self.put(path, writer=_writer, force=True)

    def copy(self, path, outpath):
        def _fetcher():
            return open(outpath, "rb").read()
        return self.put(path, fetcher=_fetcher)

    def open(self, path):
        def _fetcher():
            raise FileNotFoundError(self.location(path))
        return self.fetch(path, fetcher=_fetcher)
