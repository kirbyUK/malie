#!/usr/bin/env python3
"""Provides the Spinarak class, which is a frontend for the Requests module."""
import json
import logging
import time
from typing import Callable
import requests

# Session -- can we re-use this even for different URLs/servers?
# Can we fall back to normal methods if headerfn isn't specified?
    # Not enough simply to omit it ... Request --> Prepare --> Send won't add headers.

class Spinarak:
    '''
    Spinarak is a frontend for the requests module, which provides fault
    tolerant methods to cope with server failures and throttling attempts.
    Given a custom headers function, Spinarak will make requests using
    the headers from that function with as few changes as possible.
    '''
    def __init__(self, retries=5, delay=30,
                 headerfn: Callable[[requests.models.Request], dict] = None):
        self.retries = retries
        self.delay = delay
        self.headerfn = headerfn

    def __fetch(self, method, url, data):
        '''fetch implementation: create a request and try to issue it.'''
        req = requests.Request(method, url=url, data=data)
        if self.headerfn:
            logging.debug("headerfn found, using it")
            req.headers = self.headerfn(req)
        else:
            logging.debug("no headerfn found, leaving this blank?")
        return self.try_send(req)

    def try_send(self, request: requests.models.Request):
        '''Send a prepared request and attempt to retry on soft error.'''
        retries = self.retries
        while retries:
            logging.info("submitting request to URL: %s", request.url)
            page = self.send(request)
            category = int(page.status_code / 100)
            if category == 2 or category == 3:
                # Success (2) or Redirects (3)
                logging.info("%d %s: %s", page.status_code, page.reason, page.url)
                return page
            elif category == 4:
                # Client error, treat as immediate error
                logging.error("%d %s: %s", page.status_code, page.reason, page.url)
                page.raise_for_status()
            elif category == 5:
                # Server error, treat as temporary error
                logging.warning("%d %s: %s", page.status_code, page.reason, page.url)
            else:
                # 100 category or unknown code
                logging.error("%d %s: %s", page.status_code, page.reason, page.url)
                page.raise_for_status()
            retries -= 1
            if retries:
                logging.debug("%d retries left, retrying in %ds ...",
                              retries, self.delay)
                time.sleep(self.delay)
        raise Exception("Could not fetch %s" % request.url)

    def send(self, request: requests.models.Request):
        '''With no error checking, prepare a request and issue it, returning the result.'''
        session = requests.Session()
        prepared = request.prepare()
        response = session.send(prepared)
        logging.debug("%s %s", response.request.method, response.request.url)
        logging.debug(json.dumps(dict(response.request.headers), indent=2))
        return response

    def fetch(self, url, data=None):
        '''Given a URL, obtain that page data, with error checking.'''
        return self.__fetch('POST' if data else 'GET', url, data)

    def poke(self, url):
        '''Check a URL to see if it exists, using custom headers.'''
        page = self.__fetch('HEAD', url, None)
        return page.status_code == 200

    def check_url(self, url):
        '''LEGACY function. Check a URL to see if it exists, ignoring custom headers.'''
        page = requests.head(url)
        return page.status_code == 200
