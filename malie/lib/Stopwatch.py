#!/usr/bin/env python3

import time

class Stopwatch():
    def __init__(self):
        self._t1 = time.process_time()

    def tick(self):
        t2 = time.process_time()
        v = t2 - self._t1
        self._t1 = t2
        return v

    def snooze(self, secs):
        delta = self.tick()
        if delta < secs:
            time.sleep(secs - delta)

    def ptick(self):
        print("%f secs" % self.tick())
