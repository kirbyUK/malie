#!/usr/bin/env python3

import asyncio
import pkg_resources

import discord
from discord.ext import commands
import random

from malie.lib import resource
from malie.scrapers.ptcgo import safarizone

description = '''An example bot to showcase the discord.ext.commands extension
module.

There are a number of utility commands being showcased here.'''
bot = commands.Bot(command_prefix='?', description=description)


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')


@bot.event
async def on_message(msg):
    print("#{}/<{}> {}".format(str(msg.channel), msg.author.name, msg.content))
    await bot.process_commands(msg)


@bot.command()
async def version(ctx):
    cfg = bot.sfz.cfg
    full = cfg['cakeVersion']
    mani = cfg.manfest_version()
    mine = resources.get_version()
    msg = f"I am running malie v{mine}, emulating PTCGO version {full}#{mani}"
    await ctx.send(msg)


def cardstr(card):
    setinfo = bot.sdm.get(card.key, {})
    abbr = setinfo.get('externalId', '')
    name = card.attr('name')
    num = card.attr('cardNoText') or card.attr('cardNo')
    issue = card.attr('issue')
    msg = f"{name} {abbr}:{num} [{issue}]"
    return bot.locale.detokenize(msg)

def single_result(card):
    cardmsg = cardstr(card)
    setinfo = bot.sdm.get(card.key, {})
    cdn = card.cdn_path(setinfo, bot.locale, ext='jpg')
    uri = f"https://cdn.malie.io/file/malie-io/art/jpg/{cdn}"
    msg = f"{uri}\n{cardmsg}"
    return msg


def multi_result(results):
    msgqueue = []
    for i, result in enumerate(results[0:10], start=1):
        msgqueue.append("{}: {}".format(i, cardstr(result)))
    if len(results) > 10:
        msgqueue.append("({} more results)".format(len(results) - 10))
    msg = ("\n".join(msgqueue))
    return msg


async def add_poll_options(dmsg, author_id, results):
    emoji_actions = []

    if len(results) > 1:
        bot.query_queue[dmsg.id] = {
            'user_id': author_id,
            'results': results,
        }
        emoji = ['1⃣', '2⃣', '3⃣', '4⃣', '5⃣', '6⃣', '7⃣', '8⃣', '9⃣', '🔟']
        for emoji_char in emoji[:len(results)]:
            emoji_actions.append(dmsg.add_reaction(emoji_char))

    if len(results) > 10:
        emoji_actions.append(dmsg.add_reaction('➡'))

    if emoji_actions:
        asyncio.gather(*emoji_actions)


@bot.command()
async def search(ctx, *query):
    results = []
    for card in bot.cards.values():
        haystack = card.search_str()
        abbr = bot.sdm.get(card.key, {}).get('externalId', '')
        haystack = haystack.format(abbr=abbr)
        haystack = bot.locale.detokenize(haystack).lower()
        tokens = [q.lower() for q in query]
        if all(t in haystack for t in tokens):
            results.append(card)

    if not results:
        msg = "No results."

    elif len(results) == 1:
        msg = single_result(results[0])

    else:
        msg = multi_result(results)

    dmsg = await ctx.send(msg)
    await add_poll_options(dmsg, ctx.author.id, results)


@bot.command()
async def query(ctx, *query):
    def _qfilter(card):
        for token in query:
            search_attr = token.split(':')[0]
            search_valu = token.split(':')[1]
            if str(card.attr(search_attr)) != str(search_valu):
                return False
        return True

    try:
        results = list(bot.cards.filter(_qfilter).values())
    except:
        await ctx.send("Malformed query.")
        return

    if not results:
        msg = "No results."

    elif len(results) == 1:
        msg = single_result(results[0])

    else:
        msg = multi_result(results)

    dmsg = await ctx.send(msg)
    await add_poll_options(dmsg, ctx.author.id, results)


@bot.event
async def on_reaction_add(reaction, user):
    qmsg = bot.query_queue.get(reaction.message.id)
    if qmsg and user.id == qmsg['user_id']:
        for react in reaction.message.reactions:
            if react.me and str(react) != str(reaction):
                print("removing {}".format(str(react)))
                #await react.remove(bot.user)
        del bot.query_queue[reaction.message.id]

        if str(reaction) == '➡':
            results = qmsg['results'][10:]
            if not results:
                # fixme: handle single result
                return
            msg = multi_result(results)
            dmsg = await reaction.message.channel.send(msg)
            await add_poll_options(dmsg, user.id, results)
        else:
            emoji = ['1⃣', '2⃣', '3⃣', '4⃣', '5⃣', '6⃣', '7⃣', '8⃣', '9⃣', '🔟']
            idx = emoji.index(str(reaction))
            card = qmsg['results'][idx]
            msg = single_result(card)
            await reaction.message.channel.send(msg)


if __name__ == '__main__':
    sfz = safarizone.get_safari_zone()
    token = sfz.cfg['discordToken']

    bot.sfz = sfz
    bot.cards = sfz.definitions.cards
    bot.locale = sfz.localizations['en_US']
    bot.sdm = sfz.sdm

    bot.query_queue = {}

    bot.run(token)
